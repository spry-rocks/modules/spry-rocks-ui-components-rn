import {RatingIcon} from './assets';
import {RatingStyle} from './RatingStyle';
import React from 'react';
import {Rating as RNRating} from 'react-native-ratings';
import {useStyles} from './Rating.styles';

interface RaringProps {
  style?: RatingStyle;
  enabled?: boolean;
  rating?: number;
  readonly?: boolean;
}

const Rating: React.FC<RaringProps> = ({enabled, style, rating, readonly}) => {
  const styles = useStyles({enabled: enabled !== false, ratingStyle: style});

  return (
    <RNRating
      style={style as never}
      type="custom"
      ratingBackgroundColor="transparent"
      ratingImage={RatingIcon}
      ratingColor="transparent"
      ratingCount={rating}
      tintColor={styles.rating?.tintColor}
      imageSize={styles.rating?.starSize}
      readonly={enabled === false || readonly === true}
    />
  );
};

export default Rating;
