import {ViewStyle} from 'react-native';

export interface TouchableOpacityStyle extends ViewStyle {
  activeOpacity?: number;
}
