import {TextStyle, ViewStyle} from 'react-native';

export interface DropdownStyle {
  container?: ViewStyle;
  text?: TextStyle;
  dropdown?: {
    container?: ViewStyle;
    item?: TextStyle;
    itemSelected?: TextStyle;
  };
}
