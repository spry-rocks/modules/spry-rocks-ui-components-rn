import {ButtonDefaultTheme, ButtonTheme} from './Button.theme';
import {ImageStyle, TextStyle, ViewStyle} from 'react-native';
import {
  TouchableHighlightStyle,
  TouchableOpacityStyle,
} from '@spryrocks/rn-components-ui-touchable';
import {ButtonStyle} from './ButtonStyle';
import {useTheme} from '@spryrocks/rn-components-theme';

export interface BaseStyles {
  horizontalContainer: ViewStyle;
  text: TextStyle;
  childrenContainer: ViewStyle;
  content: {};
  leftImage: ImageStyle;
}

const useBaseStyles = ({
  style,
  theme,
  enabled,
}: {
  style: ButtonStyle | undefined;
  theme: ButtonDefaultTheme | undefined;
  enabled: boolean;
}): BaseStyles => ({
  horizontalContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  } as ViewStyle,
  content: {},
  childrenContainer: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
  } as ViewStyle,
  text: {
    ...theme?.text,
    ...style?.text,
    ...(!enabled && theme?.textDisabled),
    ...(!enabled && style?.textDisabled),
  } as TextStyle,
  leftImage: {
    ...theme?.leftImage,
    ...style?.leftImage,
  },
});

const useDefaultStyles = ({
  enabled,
  style,
}: {
  enabled: boolean;
  style: ButtonStyle | undefined;
}) => {
  const theme = useTheme<ButtonTheme>().button?.default;
  const baseStyles = useBaseStyles({enabled, theme, style});
  return {
    ...baseStyles,
    container: {
      paddingVertical: 12,
      paddingHorizontal: 30,
      alignItems: 'center',
      justifyContent: 'center',
      ...theme?.container,
      ...style?.container,
      ...(!enabled && style?.containerDisabled),
      ...(!enabled && theme?.containerDisabled),
    } as ViewStyle,
    touchableHighlight: {
      underlayColor: '#666666',
      ...theme?.touchableHighlight,
    } as TouchableHighlightStyle,
  };
};

const useRectStyles = ({
  style,
  enabled,
}: {
  style: ButtonStyle | undefined;
  enabled: boolean;
}) => {
  const theme = useTheme<ButtonTheme>().button?.rect;
  const baseStyles = useBaseStyles({enabled, theme, style});
  return {
    ...baseStyles,
    container: {
      paddingVertical: 16,
      paddingHorizontal: 30,
      alignItems: 'center',
      justifyContent: 'center',
      ...style?.container,
      ...theme?.container,
    } as ViewStyle,
    touchableHighlight: {
      underlayColor: '#666666',
      ...theme?.touchableHighlight,
    } as TouchableHighlightStyle,
  };
};

const useSimpleStyles = ({
  style,
  enabled,
}: {
  style: ButtonStyle | undefined;
  enabled: boolean;
}) => {
  const theme = useTheme<ButtonTheme>().button?.simple;
  const baseStyles = useBaseStyles({enabled, theme, style});
  return {
    ...baseStyles,
    touchableOpacity: {
      activeOpacity: 0.8,
      ...theme?.touchableOpacity,
      ...style?.container,
    } as TouchableOpacityStyle,
  };
};

export {useDefaultStyles, useSimpleStyles, useRectStyles};
