import {ImageStyle, TextStyle, ViewStyle} from 'react-native';
import {
  TextInputPlaceholderStyle,
  TextInputStyle,
  TextInputTextStyle,
} from './TextInputStyle';
import {useTheme} from '@spryrocks/rn-components-theme';
import {TextInputTheme} from './TextInput.theme';

const useBaseStyles = () => ({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  } as ViewStyle,
  textInput: {
    flex: 1,
    padding: 0,
    alignSelf: 'stretch',
  } as TextInputTextStyle,
  textInputFocused: {} as TextInputTextStyle,
  errorText: {} as TextStyle,
  leftImage: {} as ImageStyle,
  placeholder: {} as TextStyle,
  placeholderFocused: {} as TextStyle,
});

const useOverrideStyles = ({
  error,
  style,
  theme,
  focused,
}: {
  error: boolean;
  style: TextInputStyle | undefined;
  theme: TextInputStyle | undefined;
  focused: boolean;
}) => ({
  container: {
    ...style?.container,
    ...(error && style?.containerError),
    ...theme?.container,
    ...(error && theme?.containerError),
    ...(focused && theme?.containerFocused),
    ...(focused && style?.containerFocused),
  } as ViewStyle,
  textInput: {
    ...style?.textInput,
    ...(error && style?.textInputError),
    ...theme?.textInput,
    ...(error && theme?.textInputError),
  } as TextInputTextStyle,
  textInputFocused: {
    ...style?.textInputFocused,
    ...(error && style?.textInputFocused),
    ...theme?.textInputFocused,
    ...(error && theme?.textInputFocused),
  } as TextInputTextStyle,
  errorText: {
    ...style?.errorText,
    ...theme?.errorText,
  } as TextStyle,
  leftImage: {
    ...style?.leftImage,
    ...theme?.leftImage,
  } as ImageStyle,
  placeholder: {
    ...theme?.placeholder,
    ...style?.placeholder,
  } as TextInputPlaceholderStyle,
  placeholderFocused: {
    ...theme?.placeholderFocused,
    ...style?.placeholderFocused,
  } as TextInputPlaceholderStyle,
});

const useDefaultStyles = ({
  error,
  style,
}: {
  error: boolean;
  style: TextInputStyle | undefined;
}) => {
  const theme = useTheme<TextInputTheme>().textInput?.default;
  const base = useBaseStyles();
  const override = useOverrideStyles({theme, style, error, focused: false});
  return {
    container: {
      ...base.container,
      ...override.container,
    } as ViewStyle,
    textInput: {
      ...base.textInput,
      ...override.textInput,
    } as TextInputTextStyle,
    leftImage: {
      ...base.leftImage,
      marginRight: 5,
      ...override.leftImage,
    } as ImageStyle,
    placeholder: {
      ...base.placeholder,
      ...override.placeholder,
    } as TextInputPlaceholderStyle,
    placeholderFocused: {
      ...base.placeholderFocused,
      ...override.placeholderFocused,
    } as TextInputPlaceholderStyle,
  };
};

const useSimpleStyles = ({
  error,
  style,
}: {
  error: boolean;
  style: TextInputStyle | undefined;
}) => {
  const theme = useTheme<TextInputTheme>().textInput?.simple;
  const base = useBaseStyles();
  const override = useOverrideStyles({theme, style, error, focused: false});
  return {
    container: {
      ...base.container,
      ...override.container,
    } as ViewStyle,
    textInput: {
      ...base.textInput,
      alignSelf: 'stretch',
      ...override.textInput,
    } as TextInputTextStyle,
    placeholder: {
      ...base.placeholder,
      ...override.placeholder,
    } as TextInputPlaceholderStyle,
    placeholderFocused: {
      ...base.placeholderFocused,
      ...override.placeholderFocused,
    } as TextInputPlaceholderStyle,
  };
};

const useOutlinedStyles = ({
  error,
  style,
  focused,
}: {
  error: boolean;
  style: TextInputStyle | undefined;
  focused: boolean;
}) => {
  const theme = useTheme<TextInputTheme>().textInput?.material;
  const base = useBaseStyles();
  const override = useOverrideStyles({theme, style, error, focused});
  return {
    container: {
      ...base.container,
      ...override.container,
    } as ViewStyle,
    verticalContainer: {
      flex: 1,
    },
    textInput: {
      container: {
        borderRadius: undefined,
        borderWidth: undefined,
        paddingHorizontal: 0,
      } as ViewStyle,
      input: {
        ...base.textInput,
        ...(focused && base.textInputFocused),
        paddingVertical: 11,
        ...override.textInput,
        ...(focused && override.textInputFocused),
      } as TextInputTextStyle,
      inputFocused: {
        ...base.textInput,
        ...base.textInputFocused,
        ...override.textInput,
        ...override.textInputFocused,
      },
      placeholder: {
        ...base.placeholder,
        paddingLeft: undefined,
        marginLeft: 0,
        ...override.placeholder,
      } as TextInputPlaceholderStyle,
      placeholderFocused: {
        ...base.placeholder,
        ...base.placeholderFocused,
        ...override.placeholder,
        ...override.placeholderFocused,
      } as TextInputPlaceholderStyle,
    },
    error: {
      text: {
        ...base.errorText,
        ...override.errorText,
      } as TextStyle,
    },
  };
};

export {useDefaultStyles, useSimpleStyles, useOutlinedStyles};
