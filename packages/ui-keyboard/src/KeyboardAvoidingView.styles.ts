import {StyleSheet} from 'react-native';

const useStyles = () =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
  });

export {useStyles};
