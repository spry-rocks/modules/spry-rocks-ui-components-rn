import {StyleSheet} from 'react-native';
import {SwiperIndicatorDefaultTheme} from './SwiperIndicator.theme';

const useItemStyles = (
  theme: SwiperIndicatorDefaultTheme | undefined,
  isActive: boolean,
) => {
  const styles = StyleSheet.create({
    container: {
      borderRadius: 3,
      backgroundColor:
        (isActive && theme?.swiperIndicator?.selectedColor) ||
        theme?.swiperIndicator?.color ||
        'black',
      height: 6,
      width: (isActive && theme?.swiperIndicator?.selectedDotWidth) || 6,
      marginHorizontal: 2,
    },
  });

  return {...styles};
};

const useStyles = (theme: SwiperIndicatorDefaultTheme | undefined) => {
  const styles = StyleSheet.create({
    container: {
      height: 15,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      ...theme?.container,
    },
  });

  return {
    ...styles,
  };
};

export {useStyles, useItemStyles};
