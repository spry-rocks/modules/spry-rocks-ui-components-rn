import {StyleSheet} from 'react-native';

const useStyles = () =>
  StyleSheet.create({
    safeArea: {
      flex: 1,
    },
  });

export {useStyles};
