import {TextStyle, ViewStyle} from 'react-native';
import {ConfirmationCodeInputStyle} from './ConfirmationCodeInputStyle';
import {useTheme} from '../theme';

const useStyles = ({style}: {style: ConfirmationCodeInputStyle | undefined}) => {
  const appTheme = useTheme();
  const theme = appTheme.confirmationCodeInput?.default;
  const textStyle = appTheme.text?.default;

  return {
    container: {
      ...theme?.container,
      ...style?.container,
    } as ViewStyle,
    item: {
      container: {
        alignItems: 'center',
        ...theme?.itemContainer,
        ...style?.itemContainer,
      } as ViewStyle,
      text: {
        textAlignVertical: 'center',
        textAlign: 'center',
        ...theme?.itemText,
        ...style?.itemText,
      } as TextStyle,
      mark: {
        height: 5,
        width: 25,
        backgroundColor: textStyle?.text?.color,
        ...theme?.itemMark,
        ...style?.itemMark,
      } as ViewStyle,
    },
  };
};

export {useStyles};
