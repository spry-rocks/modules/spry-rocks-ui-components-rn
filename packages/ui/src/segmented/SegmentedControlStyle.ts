import {ViewStyle} from 'react-native';

export interface SegmentedControlStyle {
  container?: ViewStyle;
}
