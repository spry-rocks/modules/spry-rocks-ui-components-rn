import {ModalInternalType, ModalStyle, SwipeDirection} from './ModalStyle';
import {Animation} from '../Animatable';
import {useTheme} from '../theme';
import {ViewStyle} from 'react-native';

const useStyles = ({
  style,
  swipeDirection,
  animationIn,
  animationOut,
}: {
  style: ModalStyle | undefined;
  swipeDirection: SwipeDirection | undefined;
  animationIn: Animation | undefined;
  animationOut: Animation | undefined;
}) => {
  const theme = useTheme().modal?.default;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const modalStyleOverrideProps: any = {};
  if (swipeDirection) modalStyleOverrideProps.swipeDirection = swipeDirection;
  if (animationIn) modalStyleOverrideProps.animationIn = animationIn;
  if (animationOut) modalStyleOverrideProps.animationOut = animationOut;

  return {
    modal: {
      margin: 0,
      justifyContent: 'center',
      alignItems: 'center',
      animationIn: 'fadeIn',
      animationOut: 'fadeOut',
      ...theme?.modal,
      ...style?.modal,
      ...modalStyleOverrideProps,
    } as ModalInternalType,
    background: {
      ...theme?.background,
      ...style?.background,
    } as ViewStyle,
    thumbVisible: !!theme?.thumb || !!style?.thumb,
    thumbContainer: {
      position: 'absolute',
      left: 0,
      top: 0,
      right: 0,
    } as ViewStyle,
    thumb: {
      alignSelf: 'center',
      width: 100,
      height: 2,
      marginTop: 5,
      ...theme?.thumb,
      ...style?.thumb,
    } as ViewStyle,
  };
};

export {useStyles};
