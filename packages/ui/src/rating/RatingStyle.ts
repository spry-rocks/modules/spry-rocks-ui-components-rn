import {ViewStyle} from 'react-native';

export interface RatingStyle extends ViewStyle {
  starSize?: number;
  tintColor?: string;
}
