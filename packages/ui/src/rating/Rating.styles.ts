import {RatingStyle} from './RatingStyle';
import {StyleSheet} from 'react-native';
import {useTheme} from '../theme';

const useStyles = ({
  enabled,
  ratingStyle,
}: {
  enabled: boolean;
  ratingStyle: RatingStyle | undefined;
}) => {
  const theme = useTheme().rating?.default;

  const styles = StyleSheet.create({
    container: {
      ...theme?.container,
    },
    rating: {},
  });

  return {
    ...styles,
    rating: {
      ...styles.rating,
      ...theme?.rating,
      ...(!enabled && theme?.ratingDisabled),
      ...ratingStyle,
    } as RatingStyle,
  };
};

export {useStyles};
