import {TextStyle, ViewStyle} from 'react-native';

export interface ConfirmationCodeInputStyle {
  container?: ViewStyle;
  itemContainer?: ViewStyle;
  itemText?: TextStyle;
  itemMark?: ViewStyle;
}
