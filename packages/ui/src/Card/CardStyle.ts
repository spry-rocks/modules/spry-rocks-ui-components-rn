import {ViewStyle} from 'react-native';

export interface CardStyle {
  container?: ViewStyle;
}
