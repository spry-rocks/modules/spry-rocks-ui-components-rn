export type {TouchableHighlightStyle} from './TouchableHighlightStyle';
export type {TouchableOpacityStyle} from './TouchableOpacityStyle';
