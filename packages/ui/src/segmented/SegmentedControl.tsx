import RNSCSegmentedControl, {
  NativeSegmentedControlIOSChangeEvent,
} from '@react-native-segmented-control/segmented-control';
import {NativeSyntheticEvent} from 'react-native';
import React from 'react';
import {SegmentedControlStyle} from './SegmentedControlStyle';
import {useStyles} from './SegmentedControl.styles';

export interface SegmentedControlProps {
  style?: SegmentedControlStyle;
  items: string[];
  selectedIndex?: number;
  enabled?: boolean;
  onIndexChanged?: (index: number) => void;
}

function SegmentedControl({
  style,
  items,
  selectedIndex,
  enabled,
  onIndexChanged,
}: SegmentedControlProps) {
  const styles = useStyles({style});

  const onChangeInternal = ({
    nativeEvent: {selectedSegmentIndex},
  }: NativeSyntheticEvent<NativeSegmentedControlIOSChangeEvent>) => {
    if (onIndexChanged) onIndexChanged(selectedSegmentIndex);
  };

  return (
    <RNSCSegmentedControl
      style={styles.container}
      values={items}
      selectedIndex={selectedIndex}
      enabled={enabled}
      onChange={onChangeInternal}
      appearance="light"
    />
  );
}

export default SegmentedControl;
