import {ImageStyle, ViewStyle} from 'react-native';
import {useTheme} from '../theme';

const useItemStyles = ({mode}: {mode: 'left' | 'right' | undefined}) => {
  const themeBase = useTheme();
  const titleBarTheme = themeBase.titleBar?.default;
  const textTheme = themeBase.text?.default;
  const leftItemsTheme = themeBase.titleBarLeftItems?.default;
  const rightItemsTheme = themeBase.titleBarRightItems?.default;
  return {
    container: {
      padding: 8,
      ...(mode === 'left' && leftItemsTheme?.container),
      ...(mode === 'right' && rightItemsTheme?.container),
    } as ViewStyle,
    icon: {
      tintColor: titleBarTheme?.title?.text?.color ?? textTheme?.text?.color,
    } as ImageStyle,
    text: {
      ...titleBarTheme?.item?.text,
    },
  };
};

export {useItemStyles};
