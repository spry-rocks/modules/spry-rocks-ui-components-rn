import {ViewStyle} from 'react-native';

export interface ScrollViewStyle {
  style: ViewStyle;
  content: ViewStyle;
}
