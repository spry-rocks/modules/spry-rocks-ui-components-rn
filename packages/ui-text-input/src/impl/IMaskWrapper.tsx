/* eslint-disable @typescript-eslint/ban-ts-comment,@typescript-eslint/no-explicit-any */
import React, {ForwardRefExoticComponent, RefAttributes} from 'react';
import {TextInputHandle, TextInputInternalProps} from '../TextInput.props';
// @ts-ignore
import {IMaskNativeMixin} from 'react-native-imask';

export const wrapWithIMask = (
  TextInput: ForwardRefExoticComponent<
    TextInputInternalProps & RefAttributes<TextInputHandle>
  >,
) => {
  const InputComponent = ({inputRef, ...props}: any) => (
    <TextInput ref={inputRef} {...props} />
  );
  const Mixin = IMaskNativeMixin(InputComponent);
  return (props: any) => {
    // eslint-disable-next-line react/destructuring-assignment
    if (!props.mask) return <TextInput {...props} />;
    return (
      <Mixin
        {...props}
        onAccept={(value: string) => props.onChangeText && props.onChangeText(value)}
        onChangeText={undefined}
      />
    );
  };
};
//tmp
