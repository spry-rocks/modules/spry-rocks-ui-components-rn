import {ModalStyle, SwipeDirection} from './ModalStyle';
import React, {useState} from 'react';
import Modal from './Modal';

type OnDismiss = () => void;
type OnVisibleChanged = (isVisible: boolean) => void;

interface UseModalProps<ModalType> {
  modalType: ModalType;
  onDismiss: OnDismiss;
}

interface BaseRenderProps<ModalType> {
  type: ModalType;
}

interface RenderModalProps<ModalType> extends BaseRenderProps<ModalType> {
  style?: ModalStyle;
  swipeDirection?: SwipeDirection;
  children: React.ReactElement;
}

interface RenderCustomProps<ModalType> extends BaseRenderProps<ModalType> {
  children: (props: {
    isVisible: boolean;
    onDismiss: OnDismiss;
    onVisibleChanged: OnVisibleChanged;
  }) => React.ReactElement;
}

export function useModals<ModalType>({modalType, onDismiss}: UseModalProps<ModalType>) {
  const [currentModalType, setCurrentModalType] = useState<ModalType>();

  const isVisible = (type: ModalType) =>
    modalType === type && (currentModalType === undefined || currentModalType === type);
  const onVisibleChanged = (type: ModalType, isVisible: boolean) => {
    setCurrentModalType(isVisible ? type : undefined);
  };

  const renderModal = ({
    type,
    children,
    swipeDirection,
    style,
  }: RenderModalProps<ModalType>) => (
    <Modal
      style={style}
      isVisible={isVisible(type)}
      onDismiss={onDismiss}
      onVisibleChanged={(isVisible) => onVisibleChanged(type, isVisible)}
      swipeDirection={swipeDirection}
    >
      {children}
    </Modal>
  );
  const renderCustom = ({type, children}: RenderCustomProps<ModalType>) =>
    children({
      isVisible: isVisible(type),
      onDismiss,
      onVisibleChanged: (isVisible) => onVisibleChanged(type, isVisible),
    });
  return {renderModal, renderCustom};
}
