import {
  BaseStyles,
  useDefaultStyles,
  useRectStyles,
  useSimpleStyles,
} from './Button.styles';
import {
  Image,
  ImageSourcePropType,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import {ButtonStyle} from './ButtonStyle';
import React from 'react';

export type VisualStyle = 'default' | 'rect' | 'simple';

interface ButtonProps {
  style?: ButtonStyle;
  onPress?: () => void;
  visualStyle?: VisualStyle;
  text?: string;
  image?: ImageSourcePropType;
  enabled?: boolean;
  leftImage?: ImageSourcePropType;
  testID?: string;
}

const Button: React.FC<ButtonProps> = ({
  style,
  visualStyle = 'default',
  onPress,
  text,
  image,
  children,
  enabled,
  leftImage,
  testID,
}) => {
  const content = ({styles}: {styles: BaseStyles}) => (
    <View style={styles.horizontalContainer}>
      {leftImage && <Image style={styles.leftImage} source={leftImage} />}
      <View>
        {image && <Image source={image} />}
        {text && <Text style={styles.text}>{text}</Text>}
        {children && <View style={styles.childrenContainer}>{children}</View>}
      </View>
    </View>
  );

  const DefaultButton = () => {
    const styles = useDefaultStyles({enabled: enabled !== false, style});

    return (
      <TouchableHighlight
        style={styles.container}
        underlayColor={styles.touchableHighlight.underlayColor}
        onPress={enabled !== false ? onPress : undefined}
        testID={testID}
      >
        {content({styles})}
      </TouchableHighlight>
    );
  };

  const RectButton = () => {
    const styles = useRectStyles({style, enabled: enabled !== false});

    return (
      <TouchableHighlight
        style={[styles.container, styles.touchableHighlight]}
        underlayColor={styles.touchableHighlight.underlayColor}
        onPress={enabled !== false ? onPress : undefined}
        testID={testID}
      >
        {content({styles})}
      </TouchableHighlight>
    );
  };

  const SimpleButton = () => {
    const styles = useSimpleStyles({style, enabled: enabled !== false});

    return (
      <TouchableOpacity
        style={styles.touchableOpacity}
        onPress={enabled !== false ? onPress : undefined}
        activeOpacity={styles.touchableOpacity.activeOpacity}
        testID={testID}
      >
        {content({styles})}
      </TouchableOpacity>
    );
  };

  switch (visualStyle) {
    case 'default':
      return <DefaultButton />;
    case 'simple':
      return <SimpleButton />;
    case 'rect':
      return <RectButton />;
  }
};

export default Button;
