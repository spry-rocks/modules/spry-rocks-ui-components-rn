import {ColorValue, ViewStyle} from 'react-native';

export interface TouchableHighlightStyle extends ViewStyle {
  underlayColor?: ColorValue;
}
