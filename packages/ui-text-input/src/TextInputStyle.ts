import {ImageStyle, TextStyle, ViewStyle} from 'react-native';

export interface TextInputTextStyle extends TextStyle {
  selectionColor?: string;
}

export interface TextInputPlaceholderStyle extends TextStyle {
  top?: number;
  left?: number;
  color?: string;
}

export interface TextInputStyle {
  container?: ViewStyle;
  containerFocused?: ViewStyle;
  containerError?: ViewStyle;
  textInput?: TextInputTextStyle;
  textInputFocused?: TextInputTextStyle;
  textInputError?: TextInputTextStyle;
  errorText?: TextStyle;
  leftImage?: ImageStyle;
  placeholder?: TextInputPlaceholderStyle;
  placeholderFocused?: TextInputPlaceholderStyle;
}
