import * as Animatable from 'react-native-animatable';
import {Animation, IterationCount} from '../Animatable';
import {Image, TouchableOpacity, View, ViewStyle} from 'react-native';
import React from 'react';
import {Text} from '@spryrocks/rn-components-ui-text';
import {TitleBarItem} from './TitleBar.model';
import {useItemStyles} from './TitleBarItems.styles';

const AnimatableTouchableOpacity = Animatable.createAnimatableComponent(TouchableOpacity);

interface TitleBarItemsProps {
  style?: ViewStyle;
  items: TitleBarItem[];
  mode?: 'left' | 'right';
}

const TitleBarItems: React.FC<TitleBarItemsProps> = ({style, items, mode}) => {
  const itemStyles = useItemStyles({mode});

  const renderItem = ({
    item,
    testID,
    animation,
    iterationCount,
  }: {
    item: TitleBarItem;
    testID: string | undefined;
    animation: Animation | undefined;
    iterationCount: IterationCount | undefined;
  }) => {
    const styles = itemStyles;
    return (
      <AnimatableTouchableOpacity
        key={item.id}
        animation={animation}
        iterationCount={iterationCount}
        style={styles.container}
        activeOpacity={0.6}
        onPress={item.onPress}
        testID={testID}
      >
        {item.icon && <Image style={styles.icon} source={item.icon} />}
        {item.text && <Text style={styles.text}>{item.text}</Text>}
      </AnimatableTouchableOpacity>
    );
  };

  return (
    <View style={style}>
      {(items ?? []).map((item) =>
        renderItem({
          item,
          testID: item.testID,
          animation: item.animation,
          iterationCount: item.iterationCount,
        }),
      )}
    </View>
  );
};

const LeftTitleBarItems: React.FC<TitleBarItemsProps> = ({style, items}) => (
  <TitleBarItems style={style} items={items} mode="left" />
);

const RightTitleBarItems: React.FC<TitleBarItemsProps> = ({style, items}) => (
  <TitleBarItems style={style} items={items} mode="right" />
);

export {TitleBarItems, LeftTitleBarItems, RightTitleBarItems};
