import {Checked, Unchecked} from './assets';
import {Image, Text, TouchableOpacity} from 'react-native';
import {CheckBoxStyle} from './CheckBoxStyle';
import React from 'react';
import {useStyles} from './CheckBox.styles';

interface CheckBoxProps {
  style?: CheckBoxStyle;
  text?: string;
  checked?: boolean;
  onChecked?: (isChecked: boolean) => void;
  enabled?: boolean;
}

const CheckBox: React.FC<CheckBoxProps> = ({
  text,
  style,
  checked,
  onChecked,
  enabled,
}) => {
  const styles = useStyles({checked: checked === true, style});

  const press = () => {
    if (enabled === false) return;
    if (onChecked) onChecked(!checked);
  };

  return (
    <TouchableOpacity
      style={styles.container}
      activeOpacity={0.7}
      onPress={enabled !== false ? press : undefined}
    >
      {checked ? (
        <Image style={styles.circle} source={styles.circle.src ?? Checked} />
      ) : (
        <Image style={styles.circle} source={styles.circle.src ?? Unchecked} />
      )}
      {text && <Text style={styles.text}>{text}</Text>}
    </TouchableOpacity>
  );
};

export default CheckBox;
