/* eslint-disable @typescript-eslint/no-shadow */
import {Image, TextInput as RNTextInput, View} from 'react-native';
import React, {forwardRef, useImperativeHandle, useRef} from 'react';
import {
  TextInputHandle,
  TextInputInternalProps,
  TextInputInternalPropsBase,
} from '../TextInput.props';
import {renderRNTextInput} from '../impl/RNTextInput';
import {useSimpleStyles} from '../TextInput.styles';

const TextInputSimpleRF: React.ForwardRefRenderFunction<
  TextInputHandle,
  TextInputInternalProps
> = ({error, style, rightImage, ...props}, ref) => {
  const styles = useSimpleStyles({error: !!error, style});
  const textInputRef = useRef<RNTextInput>();
  useImperativeHandle(ref, () => ({
    focus: () => {
      if (textInputRef.current) textInputRef.current?.focus();
    },
    isFocused: () => {
      if (textInputRef.current) return textInputRef.current.isFocused();
      return false;
    },
    setNativeProps: (_props: object) => {
      // if (textInputRef.current) textInputRef.current.setNativeProps(props); // todo: restore
    },
  }));
  return (
    <View style={styles.container}>
      {renderRNTextInput({
        ...(props as TextInputInternalPropsBase),
        style: {
          input: styles.textInput,
          placeholder: styles.placeholder,
        },
        textInputRef: (ref) => {
          textInputRef.current = ref;
        },
      })}
      {rightImage && <Image source={rightImage} />}
    </View>
  );
};

export default forwardRef(TextInputSimpleRF);
