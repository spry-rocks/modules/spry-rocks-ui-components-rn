/* eslint-disable @typescript-eslint/no-shadow */
import {TextStyle, View} from 'react-native';
import React from 'react';
import {Text} from '@spryrocks/rn-components-ui-text';
import {TitleBarItem} from './TitleBar.model';
import {TitleBarItems} from './TitleBarItems';
import {TitleBarStyle} from './TitleBarStyle';
import {useStyles} from './TitleBar.styles';

export type RenderTitle = (props: {
  title: string | undefined;
  style: TextStyle;
}) => React.ReactElement;

interface TitleBarProps {
  style?: TitleBarStyle;
  title?: string;
  leftItems?: TitleBarItem[];
  rightItems?: TitleBarItem[];
  renderTitle?: RenderTitle;
  testID?: string;
}

const TitleBar: React.FC<TitleBarProps> = ({
  title,
  leftItems,
  rightItems,
  style,
  renderTitle,
  testID,
}) => {
  const styles = useStyles({style});

  const renderTitleInternal = ({
    title,
    renderTitle,
    style,
  }: {
    title: string | undefined;
    renderTitle: RenderTitle | undefined;
    style: TextStyle;
  }) => {
    if (title || renderTitle) {
      return (
        <View style={styles.titleContainer}>
          {renderTitle ? renderTitle({title, style}) : <Text style={style}>{title}</Text>}
        </View>
      );
    }
    return undefined;
  };

  return (
    <View style={styles.container} testID={testID}>
      {renderTitleInternal({title, renderTitle, style: styles.title})}
      <TitleBarItems style={styles.leftContainer} items={leftItems ?? []} />
      <TitleBarItems style={styles.rightContainer} items={rightItems ?? []} />
    </View>
  );
};

export default TitleBar;
