import {StatusBarStyle} from './StatusBarStyle';
import {useTheme} from '../theme';

const useStyles = ({style}: {style: StatusBarStyle | undefined}) => {
  const theme = useTheme().statusBar;
  return {
    statusBar: {
      barStyle: 'light-content',
      ...theme,
      ...style,
    } as StatusBarStyle,
  };
};

export {useStyles};
