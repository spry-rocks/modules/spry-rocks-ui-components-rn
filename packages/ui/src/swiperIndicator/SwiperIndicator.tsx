import {useItemStyles, useStyles} from './SwiperIndicator.styles';
import {View, ViewStyle} from 'react-native';
import React from 'react';
import {useTheme} from '../theme';

interface SwiperIndicatorProps {
  style?: ViewStyle;
  dotsCount: number;
  selectedDot: number;
}

const SwiperIndicator: React.FC<SwiperIndicatorProps> = ({
  style,
  dotsCount,
  selectedDot,
}) => {
  const theme = useTheme().swiperIndicator;
  const styles = useStyles(theme?.default);

  const Item = ({isActive}: {isActive: boolean}) => {
    const itemStyles = useItemStyles(theme?.default, isActive);

    return <View style={itemStyles.container} />;
  };

  const model: {selected: boolean}[] = [];

  for (let i = 0; i < dotsCount; i += 1) {
    model.push({selected: i === selectedDot});
  }

  return (
    <View style={[styles.container, style]}>
      {model.map((item, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <Item key={index} isActive={item.selected} />
      ))}
    </View>
  );
};

export default SwiperIndicator;
