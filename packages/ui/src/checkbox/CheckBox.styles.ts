import {CheckBoxCircleStyle, CheckBoxStyle} from './CheckBoxStyle';
import {TextStyle, ViewStyle} from 'react-native';
import {useTheme} from '../theme';

const useStyles = ({
  checked,
  style,
}: {
  checked: boolean;
  style: CheckBoxStyle | undefined;
}) => {
  const theme = useTheme().checkBox?.default;

  return {
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      ...theme?.container,
      ...style?.container,
    } as ViewStyle,
    circle: {
      ...theme?.circle,
      ...(checked && theme?.circleChecked),
      ...style?.circle,
      ...(checked && style?.circleChecked),
    } as CheckBoxCircleStyle,
    text: {
      marginLeft: 10,
      ...theme?.text,
      ...(checked && theme?.textChecked),
      ...style?.text,
      ...(checked && style?.textChecked),
    } as TextStyle,
  };
};

export {useStyles};
