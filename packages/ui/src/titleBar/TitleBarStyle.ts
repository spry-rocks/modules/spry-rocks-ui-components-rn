import {TextStyle, ViewStyle} from 'react-native';

export interface TitleBarTitleStyle {
  text?: TextStyle;
}

export interface TitleBarItemStyle {
  text?: TextStyle;
}

export interface TitleBarStyle {
  container?: ViewStyle;
  title?: TitleBarTitleStyle;
  item?: TitleBarItemStyle;
}

export interface TitleBarItemsStyle {
  container?: ViewStyle;
  item?: TitleBarItemStyle;
}
