import {DropdownStyle} from './DropdownStyle';

export interface DropdownTheme {
  default?: DropdownStyle;
}
