import {TextStyle, ViewStyle} from 'react-native';
import {TitleBarStyle} from './TitleBarStyle';
import {useTheme} from '../theme';

const createContainerBaseStyle = (position: 'left' | 'right'): ViewStyle => ({
  position: 'absolute',
  [position]: 12,
  flexDirection: 'row',
  alignItems: 'center',
});

const useStyles = ({style}: {style: TitleBarStyle | undefined}) => {
  const theme = useTheme().titleBar?.default;
  return {
    container: {
      padding: 13,
      justifyContent: 'center',
      ...theme?.container,
      ...style?.container,
    } as ViewStyle,
    titleContainer: {
      alignSelf: 'center',
    } as ViewStyle,
    leftContainer: {
      ...createContainerBaseStyle('left'),
    } as ViewStyle,
    rightContainer: {
      ...createContainerBaseStyle('right'),
    } as ViewStyle,
    title: {
      ...theme?.title?.text,
    } as TextStyle,
  };
};

export {useStyles};
