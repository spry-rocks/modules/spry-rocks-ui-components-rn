import {
  createTheme,
  Theme,
  useTheme as useThemeCore,
} from '@spryrocks/rn-components-theme';
import {TitleBarItemsTheme, TitleBarTheme} from './titleBar';
import {ActivityIndicatorTheme} from './activityIndicator';
import {ButtonTheme} from '@spryrocks/rn-components-ui-button';
import {CardTheme} from './Card';
import {CheckBoxTheme} from './checkbox';
import {ConfirmationCodeInputTheme} from './comfirmationCodeInput';
import {DropdownTheme} from './dropdown';
import {ModalTheme} from './modal';
import {RatingTheme} from './rating';
import {SegmentedControlTheme} from './segmented';
import {StatusBarTheme} from './StatusBar';
import {SwiperIndicatorTheme} from './swiperIndicator';
import {SwiperTheme} from './Swiper';
import {SwitchTheme} from '@spryrocks/rn-components-ui-switch';
import {TextInputTheme} from '@spryrocks/rn-components-ui-text-input';
import {TextTheme} from '@spryrocks/rn-components-ui-text';

export interface ComponentsTheme
  extends Theme,
    TextTheme,
    ButtonTheme,
    TextInputTheme,
    SwitchTheme {
  checkBox?: CheckBoxTheme;
  titleBar?: TitleBarTheme;
  titleBarLeftItems?: TitleBarItemsTheme;
  titleBarRightItems?: TitleBarItemsTheme;
  swiperIndicator?: SwiperIndicatorTheme;
  rating?: RatingTheme;
  modal?: ModalTheme;
  dropdown?: DropdownTheme;
  activityIndicator?: ActivityIndicatorTheme;
  statusBar?: StatusBarTheme;
  confirmationCodeInput?: ConfirmationCodeInputTheme;
  segmentedControl?: SegmentedControlTheme;
  card?: CardTheme;
  swiper?: SwiperTheme;
}

const componentsTheme = createTheme<ComponentsTheme>({
  button: {
    default: {
      container: {
        minHeight: 50,
        backgroundColor: 'gray',
        borderRadius: 10,
      },
      touchableHighlight: {
        underlayColor: '#666666',
      },
    },
    simple: {
      touchableOpacity: {
        activeOpacity: 0.6,
      },
    },
  },
  card: {
    default: {
      container: {
        backgroundColor: 'white',
      },
    },
  },
});

export const useTheme = <T extends ComponentsTheme>() => useThemeCore<T>();

export {componentsTheme};
