import {
  Animation as RNAAnimation,
  CustomAnimation as RNACustomAnimation,
} from 'react-native-animatable';
import {Direction} from 'react-native-modal';
import {ViewStyle} from 'react-native';

export type SwipeDirection = Direction | Array<Direction>;
export type Animation = RNAAnimation | RNACustomAnimation;

export interface ModalInternalType extends ViewStyle {
  swipeDirection?: SwipeDirection;
  animationIn?: Animation;
  animationOut?: Animation;
}

export interface ModalStyle {
  background?: ViewStyle;
  thumb?: ViewStyle;
  modal?: ModalInternalType;
}
