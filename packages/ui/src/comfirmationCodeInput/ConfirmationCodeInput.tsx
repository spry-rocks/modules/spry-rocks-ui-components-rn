import {
  CodeField,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import {ConfirmationCodeInputStyle} from './ConfirmationCodeInputStyle';
import React from 'react';
import {Text} from '@spryrocks/rn-components-ui-text';
import {useStyles} from './ConfirmationCodeInput.styles';
import {View} from 'react-native';

interface VerificationCodeInputProps {
  style?: ConfirmationCodeInputStyle;
  length?: number;
  value?: string;
  onValueChanged?: (value: string) => void;
  autoFocus?: boolean;
  testID?: string;
}

const ConfirmationCodeInput: React.FC<VerificationCodeInputProps> = ({
  length = 4,
  style,
  value,
  onValueChanged,
  autoFocus,
  testID,
}) => {
  const styles = useStyles({style});

  const setValue = (value: string) => {
    if (onValueChanged) onValueChanged(value);
  };

  const ref = useBlurOnFulfill({value, cellCount: length});
  const [clearByFocusCellProps, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  return (
    <CodeField
      ref={ref}
      rootStyle={styles.container}
      {...clearByFocusCellProps}
      autoFocus={autoFocus}
      value={value}
      onChangeText={onValueChanged}
      cellCount={length}
      keyboardType="number-pad"
      textContentType="oneTimeCode"
      renderCell={({index, symbol}) => (
        <View
          style={styles.item.container}
          key={index}
          onLayout={getCellOnLayoutHandler(index)}
        >
          <Text style={styles.item.text}>{symbol}</Text>
          <View style={styles.item.mark} />
        </View>
      )}
      testID={testID}
    />
  );
};

export default ConfirmationCodeInput;
