import {Animation, ModalStyle, SwipeDirection} from './ModalStyle';
import React from 'react';
import RNModal from 'react-native-modal';
import {useStyles} from './Modal.styles';
import {View} from 'react-native';

interface ModalProps {
  style?: ModalStyle;
  isVisible: boolean;
  onDismiss?: () => void;
  onVisibleChanged?: (isVisible: boolean) => void;
  swipeDirection?: SwipeDirection;
  animationIn?: Animation;
  animationOut?: Animation;
}

const Modal: React.FC<ModalProps> = ({
  style,
  isVisible,
  children,
  onDismiss,
  onVisibleChanged,
  swipeDirection,
  animationIn,
  animationOut,
}) => {
  const styles = useStyles({
    style,
    swipeDirection,
    animationIn,
    animationOut,
  });

  const renderThumb = () => {
    if (!styles.thumbVisible) return undefined;

    return (
      <View style={styles.thumbContainer} pointerEvents="none">
        <View style={styles.thumb} />
      </View>
    );
  };

  return (
    <RNModal
      style={styles.modal}
      isVisible={isVisible}
      swipeDirection={styles.modal.swipeDirection}
      onSwipeComplete={() => onDismiss && onDismiss()}
      avoidKeyboard
      onBackdropPress={() => onDismiss && onDismiss()}
      onModalShow={() => onVisibleChanged && onVisibleChanged(true)}
      onModalHide={() => onVisibleChanged && onVisibleChanged(false)}
      useNativeDriver
      propagateSwipe
      animationIn={styles.modal.animationIn}
      animationOut={styles.modal.animationOut}
    >
      <View style={styles.background}>
        {children}
        {renderThumb()}
      </View>
    </RNModal>
  );
};

export default Modal;
