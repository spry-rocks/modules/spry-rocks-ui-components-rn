import {ImageStyle, TextStyle, ViewStyle} from 'react-native';
import {DropdownStyle} from './DropdownStyle';
import {useTheme} from '../theme';

const useStyles = ({style}: {style: DropdownStyle | undefined}) => {
  const baseTheme = useTheme();
  const theme = baseTheme.dropdown?.default;
  return {
    container: {
      justifyContent: 'center',
      ...style?.container,
      ...theme?.container,
    } as ViewStyle,
    content: {
      container: {
        flexDirection: 'row',
        alignItems: 'center',
      } as ViewStyle,
      text: {
        ...style?.text,
        ...theme?.text,
      } as TextStyle,
      stub: {flex: 1},
      arrow: {
        marginLeft: 6,
        tintColor: theme?.text?.color ?? style?.text?.color ?? 'black',
      } as ImageStyle,
    },
    dropdown: {
      container: {
        padding: 5,
        ...style?.dropdown?.container,
        ...theme?.dropdown?.container,
      } as ViewStyle,
      item: {
        backgroundColor: undefined,
        borderColor: undefined,
        ...style?.dropdown?.item,
        ...theme?.dropdown?.item,
      } as TextStyle,
      itemSelected: {
        backgroundColor: undefined,
        borderColor: undefined,
        ...style?.dropdown?.item,
        ...theme?.dropdown?.item,
        ...style?.dropdown?.itemSelected,
        ...theme?.dropdown?.itemSelected,
      } as TextStyle,
    },
  };
};

export {useStyles};
