import {TextStyle} from 'react-native';
import {TextTheme} from './Text.theme';
import {useTheme} from '@spryrocks/rn-components-theme';

const useStyles = ({style}: {style: TextStyle | undefined}) => {
  const theme = useTheme<TextTheme>().text?.default;
  return {
    text: {
      ...theme?.text,
      ...style,
    } as TextStyle,
  };
};

export {useStyles};
