import {ModalStyle} from './ModalStyle';

export interface ModalTheme {
  default?: ModalStyle;
}
