import {SegmentedControlStyle} from './SegmentedControlStyle';
import {useTheme} from '../theme';

const useStyles = ({style}: {style: SegmentedControlStyle | undefined}) => {
  const theme = useTheme().segmentedControl?.default;

  return {
    container: {
      ...theme?.container,
      ...style?.container,
    },
  };
};

export {useStyles};
