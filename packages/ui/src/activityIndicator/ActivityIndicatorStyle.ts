import {ViewStyle} from 'react-native';

export type ActivityIndicatorSize = number | 'small' | 'large';
export type ActivityIndicatorIndicatorStyle = {
  color?: string;
  size?: ActivityIndicatorSize;
} & ViewStyle;
export type ActivityIndicatorBlurStyle = {
  blurType?:
    | 'xlight'
    | 'light'
    | 'dark'
    // iOS 13+ only
    | 'chromeMaterial'
    | 'material'
    | 'thickMaterial'
    | 'thinMaterial'
    | 'ultraThinMaterial'
    | 'chromeMaterialDark'
    | 'materialDark'
    | 'thickMaterialDark'
    | 'thinMaterialDark'
    | 'ultraThinMaterialDark'
    | 'chromeMaterialLight'
    | 'materialLight'
    | 'thickMaterialLight'
    | 'thinMaterialLight'
    | 'ultraThinMaterialLight'
    // tvOS and iOS 10+ only
    | 'regular'
    | 'prominent'
    // tvOS only
    | 'extraDark';
  blurAmount?: number; // 0 - 100
  reducedTransparencyFallbackColor?: string;
  blurRadius?: number;
  downsampleFactor?: number;
  overlayColor?: string;
};

export interface ActivityIndicatorStyle {
  container?: ViewStyle;
  indicator?: ActivityIndicatorIndicatorStyle;
  blur?: ActivityIndicatorBlurStyle;
}
