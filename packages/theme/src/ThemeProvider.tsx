import React, {PropsWithChildren, useContext} from 'react';
import {mergeThemes} from './utils';
import Theme from './Theme';

const ThemeContext = React.createContext<Theme>({});

interface ThemeProviderProps<T> {
  theme?: T;
  override?: T;
}

export function useTheme<T extends Theme>(): T {
  return useContext(ThemeContext) as T;
}

function ThemeProvider({
  theme,
  override,
  children,
}: PropsWithChildren<ThemeProviderProps<Theme>>) {
  const baseTheme = useTheme();

  return (
    <ThemeContext.Provider value={mergeThemes(theme || baseTheme, override || {})}>
      {children}
    </ThemeContext.Provider>
  );
}

export default ThemeProvider;
