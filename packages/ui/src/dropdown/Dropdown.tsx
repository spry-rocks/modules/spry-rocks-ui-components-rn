import {Image, TouchableOpacity, View} from 'react-native';
import {Arrow} from './assets';
import {DropdownStyle} from './DropdownStyle';
import ModalDropdown from 'react-native-modal-dropdown';
import React from 'react';
import {Text} from '@spryrocks/rn-components-ui-text';
import {useStyles} from './Dropdown.styles';

interface DropdownProps<T> {
  testID?: string;
  style?: DropdownStyle;
  values: {text: string; value: T; testID?: string}[];
  selectedIndex: number;
  onSelected?: (item: {value: T; index: number}) => void;
  placeholder?: string;
}

function Dropdown<T>({
  testID,
  style,
  values,
  selectedIndex,
  onSelected,
  placeholder,
}: DropdownProps<T>) {
  const styles = useStyles({style});

  const selectedValue = values[selectedIndex];

  return (
    <ModalDropdown
      testID={testID}
      style={styles.container}
      dropdownStyle={styles.dropdown.container}
      dropdownTextStyle={styles.dropdown.item}
      dropdownTextHighlightStyle={styles.dropdown.itemSelected}
      options={values.map((v) => v.text)}
      renderSeparator={() => <View />}
      defaultIndex={1}
      onSelect={(index: number) => {
        const selectedValue = values[index];
        const {value} = selectedValue;
        if (onSelected) onSelected({value, index});
      }}
      renderRowComponent={(item: {children: React.ReactElement}) => (
        <TouchableOpacity {...item} />
      )}
      renderRow={(text: string, index: number, _selected: boolean) => (
        <Text testID={values[index].testID}>{text}</Text>
      )}
    >
      <View style={styles.content.container}>
        <Text style={styles.content.text}>
          {selectedValue ? selectedValue.text : placeholder ?? ''}
        </Text>
        <View style={styles.content.stub} />
        <Image style={styles.content.arrow} source={Arrow} />
      </View>
    </ModalDropdown>
  );
}

export default Dropdown;
