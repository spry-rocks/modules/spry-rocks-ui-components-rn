import {Animation, IterationCount} from '../Animatable';
import {ImageSourcePropType} from 'react-native';

export interface TitleBarItem {
  id: string;
  text?: string;
  icon?: ImageSourcePropType;
  onPress?: () => void;
  testID?: string;
  animation?: Animation;
  iterationCount?: IterationCount;
}
