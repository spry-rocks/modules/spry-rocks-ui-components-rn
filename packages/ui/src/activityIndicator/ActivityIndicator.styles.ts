import {
  ActivityIndicatorBlurStyle,
  ActivityIndicatorIndicatorStyle,
  ActivityIndicatorSize,
  ActivityIndicatorStyle,
} from './ActivityIndicatorStyle';
import {useTheme} from '../theme';
import {ViewStyle} from 'react-native';

const useStyles = ({
  style,
  foreground,
  size,
}: {
  style: ActivityIndicatorStyle | undefined;
  foreground: boolean;
  size: ActivityIndicatorSize | undefined;
}) => {
  const theme = useTheme().activityIndicator?.default;
  return {
    container: {
      ...style?.container,
      ...(foreground &&
        ({
          position: 'absolute',
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
        } as ViewStyle)),
      justifyContent: 'center',
      alignItems: 'center',
      ...theme?.container,
    } as ViewStyle,
    indicator: {
      ...style?.indicator,
      ...theme?.indicator,
      ...(size &&
        ({
          size,
        } as ActivityIndicatorIndicatorStyle)),
    } as ActivityIndicatorIndicatorStyle,
    blur: {
      ...theme?.blur,
      ...style?.blur,
    } as ActivityIndicatorBlurStyle,
    blurContainer: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    } as ViewStyle,
  };
};

export {useStyles};
