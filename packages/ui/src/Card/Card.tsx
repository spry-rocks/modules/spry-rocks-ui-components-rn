import {CardStyle} from './CardStyle';
import React from 'react';
import {useStyles} from './Card.styles';
import {View} from 'react-native';

interface CardProps {
  style?: CardStyle;
}

const Cart: React.FC<CardProps> = ({children, style}) => {
  const styles = useStyles({style});

  return <View style={styles.container}>{children}</View>;
};

export default Cart;
