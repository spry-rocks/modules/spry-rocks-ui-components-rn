import {ImageSourcePropType, ImageStyle, TextStyle, ViewStyle} from 'react-native';

export interface CheckBoxCircleStyle extends ImageStyle {
  src?: ImageSourcePropType;
}

export interface CheckBoxStyle {
  container?: ViewStyle;
  circle?: CheckBoxCircleStyle;
  circleChecked?: CheckBoxCircleStyle;
  text?: TextStyle;
  textChecked?: TextStyle;
}
