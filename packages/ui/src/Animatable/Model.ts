import {
  Animation as RNAAnimation,
  CustomAnimation as RNACustomAnimation,
} from 'react-native-animatable';

export type Animation = RNAAnimation | RNACustomAnimation | string;
export type IterationCount = number | 'infinite';
