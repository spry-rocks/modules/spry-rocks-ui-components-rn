import {TextStyle, ViewStyle} from 'react-native';
import {CheckBoxCircleStyle} from './CheckBoxStyle';

export interface CheckBoxDefaultTheme {
  container?: ViewStyle;
  circle?: CheckBoxCircleStyle;
  circleChecked?: CheckBoxCircleStyle;
  text?: TextStyle;
  textChecked?: TextStyle;
}

export interface CheckBoxTheme {
  default?: CheckBoxDefaultTheme;
}
