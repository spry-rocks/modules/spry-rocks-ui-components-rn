import {SegmentedControlStyle} from './SegmentedControlStyle';

export interface SegmentedControlTheme {
  default?: SegmentedControlStyle;
}
