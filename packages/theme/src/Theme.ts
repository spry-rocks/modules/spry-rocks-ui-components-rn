type Theme = {[key: string]: unknown};
export default Theme;

export const createTheme = <T extends Theme>(theme: T) => theme;

export type WithVariants<TBaseTheme, TVariant extends string> = TBaseTheme & {
  variant?: {
    [key in TVariant]?: TBaseTheme;
  };
};
