import React from 'react';
import RNSwitch from 'react-native-switch-pro';
import {useStyles} from './Switch.styles';

interface SwitchProps {
  checked?: boolean;
  onCheckedChanged?: (checked: boolean) => void;
  enabled?: boolean;
}

const Switch: React.FC<SwitchProps> = ({checked, onCheckedChanged, enabled}) => {
  const styles = useStyles();

  return (
    <RNSwitch
      style={styles.switch}
      width={styles.switch.width}
      height={styles.switch.height}
      backgroundActive={styles.switchActive?.backgroundColor}
      value={checked}
      onAsyncPress={(callback: (result: boolean) => void) => {
        callback(false);
        if (onCheckedChanged) onCheckedChanged(!checked);
      }}
      disabled={enabled !== undefined && !enabled}
    />
  );
};

export default Switch;
