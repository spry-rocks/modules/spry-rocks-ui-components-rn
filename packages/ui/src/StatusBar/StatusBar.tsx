import React from 'react';
import {StatusBar as RNStatusBar} from 'react-native';
import {StatusBarStyle} from './StatusBarStyle';
import {useStyles} from './StatusBar.styles';

interface StatusBarProps {
  style?: StatusBarStyle;
}

const StatusBar: React.FC<StatusBarProps> = ({style}) => {
  const styles = useStyles({style});

  return (
    <RNStatusBar
      translucent
      backgroundColor="transparent"
      barStyle={styles.statusBar.barStyle}
    />
  );
};

export default StatusBar;
