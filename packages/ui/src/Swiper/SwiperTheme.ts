import {SwiperStyle} from './SwiperStyle';

export interface SwiperTheme {
  default?: SwiperStyle;
}
