import {ActivityIndicatorSize, ActivityIndicatorStyle} from './ActivityIndicatorStyle';
import {ActivityIndicator as RNActivityIndicator, View} from 'react-native';
import {BlurView} from '@react-native-community/blur';
import React from 'react';
import {useStyles} from './ActivityIndicator.styles';

type PointerEvents = 'box-none' | 'none' | 'box-only' | 'auto';

interface ActivityIndicatorProps {
  style?: ActivityIndicatorStyle;
  foreground?: boolean;
  size?: ActivityIndicatorSize;
  pointerEvents?: PointerEvents;
  blur?: boolean;
}

const ActivityIndicator: React.FC<ActivityIndicatorProps> = ({
  style,
  foreground,
  size,
  pointerEvents,
  blur,
}) => {
  const styles = useStyles({style, foreground: foreground === true, size});

  return (
    <View style={styles.container} pointerEvents={pointerEvents}>
      {blur && (
        <BlurView
          style={styles.blurContainer}
          blurAmount={styles.blur.blurAmount}
          blurRadius={styles.blur.blurRadius}
          blurType={styles.blur.blurType}
          downsampleFactor={styles.blur.downsampleFactor}
          overlayColor={styles.blur.overlayColor}
          reducedTransparencyFallbackColor={styles.blur.reducedTransparencyFallbackColor}
        />
      )}
      <RNActivityIndicator
        style={styles.indicator}
        color={styles.indicator.color}
        size={styles.indicator.size}
      />
    </View>
  );
};

export default ActivityIndicator;
