import {StyleSheet} from 'react-native';

const useStyles = (itemSize: number) => {
  const styles = StyleSheet.create({
    scrollView: {
      alignSelf: 'center',
      height: itemSize,
    },
    centerItemIndicatorContainer: {
      // backgroundColor: 'blue',
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      alignItems: 'center',
    },
    centerItemIndicatorBlock: {
      backgroundColor: 'yellow',
      width: 1,
      flex: 1,
    },
    scrollViewContentContainer: {
      flexDirection: 'row',
    },
  });
  return {
    ...styles,
    extraItem: (extraItemsCount: number) => ({
      width: extraItemsCount * itemSize,
    }),
  };
};

export {useStyles};
