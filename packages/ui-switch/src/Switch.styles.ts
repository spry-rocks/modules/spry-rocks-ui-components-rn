import {SwitchStyle} from './SwitchStyle';
import {SwitchTheme} from './Switch.theme';
import {useTheme} from '@spryrocks/rn-components-theme';

const useStyles = () => {
  const theme = useTheme<SwitchTheme>().switch?.default;

  return {
    switch: {
      ...theme?.switch,
    } as SwitchStyle,
    switchActive: {
      ...theme?.switchActive,
    } as SwitchStyle,
  };
};

export {useStyles};
