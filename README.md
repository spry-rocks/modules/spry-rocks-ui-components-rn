This library help developer create UI in react native apps. Components have not default look, otherwise developer can setup themes and styles, but library should give usual functionality and components should be open to modifications as well.

## Installation

`yarn add @spryrocks/rn-components-theme @spryrocks/rn-components-ui`

Optionally you may install next dependencies:

- `yarn add react-native-floating-label-input`
to use `TextInput` component with `visualStyle="material"` prop
([Cnilton/react-native-floating-label-input](https://github.com/Cnilton/react-native-floating-label-input))
- `yarn add react-native-modal`
to use `Modal` component
([react-native-modal/react-native-modal](https://github.com/react-native-modal/react-native-modal))
- `yarn add react-native-ratings`
to use `Rating` component
([Monte9/react-native-ratings](https://github.com/Monte9/react-native-ratings))
- `yarn add react-native-reanimated`
to use `Text` component with `animated={true}` prop
([software-mansion/react-native-reanimated](https://github.com/software-mansion/react-native-reanimated))
- `yarn add react-native-safe-area-context`
to use `SafeAreaView` component
([th3rdwave/react-native-safe-area-context](https://github.com/th3rdwave/react-native-safe-area-context))
- `yarn add react-native-switch-pro`
to use `Switch` component
([poberwong/react-native-switch-pro](https://github.com/poberwong/react-native-switch-pro))
- `yarn add react-native-modal-dropdown`
to use `Dropdown` component
([sohobloo/react-native-modal-dropdown](https://github.com/sohobloo/react-native-modal-dropdown))

## Supported components

- ActivityIndicator
- Button
- CheckBox
- Dropdown
- HorizontalPicker
- KeyboardAvoidingView
- Modal
- Rating
- SafeAreaView
- StatusBar
- SwiperIndicator
- Switch
- Text
- TextInput
- TitleBar

## Themes

App should declare root theme and helper functions, for example you can add these lines to the `theme/Theme.ts` file:

```
import {
   useTheme as useThemeBase,
   createTheme as createThemeBase,
   Theme as BaseTheme,
 } from '@spryrocks/rn-components-theme';
import {ComponentsTheme} from '@spryrocks/rn-components-ui';

export interface Theme extends BaseTheme, ComponentsTheme {}
export const useTheme = <T extends RootTheme>() => useThemeBase<T>();
export const createTheme = <T extends RootTheme>(theme: T) => createThemeBase<T>(theme);
```

You can apply the root theme, for example in the `RootTheme.ts`:

```
import {createTheme} from './Theme';
import {RootTheme} from 'routes';

export const rootTheme = createTheme<RootTheme>({
  statusBar: {
    barStyle: 'light-content',
  },
  button: {
    default: {
      container: {
        backgroundColor: 'red',
      },
    },
    ...
  },
  ...
});
```

And then theme should be applied at the root level:

```
import {rootTheme} from 'theme/RootTheme';

<ThemeProvider theme={rootTheme}>
  {the app's content}
</ThemeProvider>
```

Then any component can be used and the theme will be applied to the components.

```
<Button text="Hello workd!" />
```

Anytime theme can be overwritten in this way:
```
import {anotherTheme} from 'theme/AnotherTheme';

<...>
  <ThemeProvider override={anotherTheme}>
    {applied another theme on top of root theme}
  </ThemeProvider>
</...>
```