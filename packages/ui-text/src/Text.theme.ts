import {TextStyle} from 'react-native';
import {Theme} from '@spryrocks/rn-components-theme';

export interface TextDefaultTheme {
  text?: TextStyle;
}

export interface TextTheme extends Theme {
  text?: {
    default?: TextDefaultTheme;
  };
}
