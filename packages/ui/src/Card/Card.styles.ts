import {CardStyle} from './CardStyle';
import {useTheme} from '../theme';
import {ViewStyle} from 'react-native';

const useStyles = ({style}: {style: CardStyle | undefined}) => {
  const theme = useTheme().card?.default;

  return {
    container: {
      borderRadius: 4,
      elevation: 1,
      shadowOffset: {
        width: 0.5,
        height: 0.5,
      },
      shadowOpacity: 0.5,
      shadowRadius: 1,
      ...theme?.container,
      ...style?.container,
    } as ViewStyle,
  };
};

export {useStyles};
