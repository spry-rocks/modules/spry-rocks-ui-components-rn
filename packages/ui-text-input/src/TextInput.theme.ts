import {TextInputStyle} from './TextInputStyle';
import {Theme} from '@spryrocks/rn-components-theme';

export interface TextInputTheme extends Theme {
  textInput?: {
    default?: TextInputStyle;
    simple?: TextInputStyle;
    material?: TextInputStyle;
  };
}
