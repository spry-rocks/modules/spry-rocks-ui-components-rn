import SwiperIndicatorStyle from './SwiperIndicatorStyle';
import {ViewStyle} from 'react-native';

export interface SwiperIndicatorDefaultTheme {
  container?: ViewStyle;
  swiperIndicator?: SwiperIndicatorStyle;
}

export interface SwiperIndicatorTheme {
  default?: SwiperIndicatorDefaultTheme;
}
