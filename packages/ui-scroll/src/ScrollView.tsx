import React from 'react';
import {ScrollView as RNScrollView} from 'react-native';
import {ScrollViewStyle} from './ScrollViewStyle';
import {useStyles} from './ScrollView.styles';

interface ScrollViewProps {
  style?: ScrollViewStyle;
}

export const ScrollView: React.FC<ScrollViewProps> = ({style, children}) => {
  const styles = useStyles({style});

  return (
    <RNScrollView style={styles.style} contentContainerStyle={styles.content}>
      {children}
    </RNScrollView>
  );
};
