import {Platform, KeyboardAvoidingView as RNKeyboardAvoidingView} from 'react-native';
import React from 'react';
import {useStyles} from './KeyboardAvoidingView.styles';

interface KeyboardAvoidingViewProps {}

const KeyboardAvoidingView: React.FC<KeyboardAvoidingViewProps> = ({children}) => {
  const styles = useStyles();

  return (
    <RNKeyboardAvoidingView
      style={styles.container}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
    >
      {children}
    </RNKeyboardAvoidingView>
  );
};

export default KeyboardAvoidingView;
