import {StatusBarStyle as RNStatusBarStyle} from 'react-native';

export interface StatusBarStyle {
  barStyle?: RNStatusBarStyle;
}
