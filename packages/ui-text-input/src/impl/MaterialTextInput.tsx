import {
  FloatingInputRef,
  FloatingLabelInput as MaterialTextInput,
} from 'react-native-floating-label-input';
import {TextInputPlaceholderStyle, TextInputTextStyle} from '../TextInputStyle';
import React from 'react';
import {TextInputInternalPropsBase} from '../TextInput.props';
import {ViewStyle} from 'react-native';

export type MaterialInputRef = FloatingInputRef;

export interface TextInputRNPProps extends TextInputInternalPropsBase {
  textInputRef: (ref: MaterialInputRef | undefined) => void;
  style: {
    container: ViewStyle;
    input: TextInputTextStyle;
    inputFocused: TextInputTextStyle;
    placeholder: TextInputPlaceholderStyle;
    placeholderFocused: TextInputPlaceholderStyle;
  };
  focused: boolean;
  onFocused: (focused: boolean) => void;
  isPassword: boolean | undefined;
}

export const renderMaterialTextInput = ({
  textInputRef,
  style,
  placeholder,
  value,
  onChangeText,
  autoCompleteType,
  textContentType,
  keyboardType,
  autoCapitalize,
  editable,
  focused,
  onFocused,
  multiline,
  returnKeyType,
  blurOnSubmit,
  onSubmitEditing,
  hint,
  staticPlaceholder,
  testID,
  isPassword,
  ...restProps
}: TextInputRNPProps) => (
  <MaterialTextInput
    ref={(ref) => textInputRef(ref ?? undefined)}
    containerStyles={style.container}
    inputStyles={style.input}
    labelStyles={style.placeholder}
    label={placeholder || ''}
    value={value}
    onChangeText={onChangeText}
    autoComplete={autoCompleteType}
    textContentType={textContentType}
    keyboardType={keyboardType}
    autoCapitalize={autoCapitalize}
    editable={editable}
    selectionColor={style.input.selectionColor}
    onFocus={() => onFocused(true)}
    onBlur={() => onFocused(false)}
    isFocused={focused || (value !== undefined && value.length > 0)}
    customLabelStyles={{
      topBlurred: style.placeholder.top,
      topFocused: style.placeholderFocused.top,
      leftBlurred: style.placeholder.left,
      leftFocused: style.placeholderFocused.left,
      fontSizeBlurred: style.placeholder.fontSize,
      fontSizeFocused: style.placeholderFocused.fontSize,
      colorBlurred: style.placeholder.color,
      colorFocused: style.placeholderFocused.color,
    }}
    multiline={multiline}
    returnKeyType={returnKeyType}
    blurOnSubmit={blurOnSubmit}
    onSubmitEditing={onSubmitEditing}
    hint={hint}
    staticLabel={staticPlaceholder}
    testID={testID}
    {...restProps}
    isPassword={isPassword}
  />
);
