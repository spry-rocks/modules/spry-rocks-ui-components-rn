import {SwiperStyle} from './SwiperStyle';
import {useTheme} from '../theme';
import {ViewStyle} from 'react-native';

const useStyles = ({style}: {style: SwiperStyle | undefined}) => {
  const theme = useTheme().swiper?.default;

  return {
    swiper: {
      ...theme?.swiper,
      ...style?.swiper,
    } as ViewStyle,
  };
};

export {useStyles};
