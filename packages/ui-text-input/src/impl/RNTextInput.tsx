import {TextInputPlaceholderStyle, TextInputTextStyle} from '../TextInputStyle';
import React from 'react';
import {TextInput as RNTextInput} from 'react-native';
import {TextInputInternalPropsBase} from '../TextInput.props';

export interface TextInputRNProps extends TextInputInternalPropsBase {
  textInputRef: (ref: RNTextInput | undefined) => void;
  style: {
    input: TextInputTextStyle;
    placeholder: TextInputPlaceholderStyle;
  };
}

export const renderRNTextInput = ({
  textInputRef,
  style,
  placeholder,
  value,
  onChangeText,
  autoCompleteType,
  textContentType,
  keyboardType,
  autoCapitalize,
  secureTextEntry,
  editable,
  multiline,
  returnKeyType,
  blurOnSubmit,
  onSubmitEditing,
  testID,
  ...restProps
}: TextInputRNProps) => (
  <RNTextInput
    ref={(ref) => textInputRef(ref ?? undefined)}
    style={style.input}
    placeholder={placeholder}
    placeholderTextColor={style.placeholder?.color}
    value={value}
    onChangeText={onChangeText}
    autoComplete={autoCompleteType}
    textContentType={textContentType}
    keyboardType={keyboardType}
    autoCapitalize={autoCapitalize}
    secureTextEntry={secureTextEntry}
    editable={editable}
    selectionColor={style.input?.selectionColor}
    multiline={multiline}
    returnKeyType={returnKeyType}
    blurOnSubmit={blurOnSubmit}
    onSubmitEditing={onSubmitEditing}
    testID={testID}
    {...restProps}
  />
);
