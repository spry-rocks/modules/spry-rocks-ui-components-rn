import {ScrollViewStyle} from './ScrollViewStyle';

interface UseStylesProps {
  style: ScrollViewStyle | undefined;
}

export const useStyles = ({style}: UseStylesProps) => {
  return {
    style: {
      ...style?.style,
    },
    content: {
      ...style?.content,
    },
  };
};
