import {AutoCapitalize, AutoCompleteType, InputType, TextContentType} from './Model';
import {
  ImageSourcePropType,
  KeyboardTypeOptions,
  NativeSyntheticEvent,
  ReturnKeyTypeOptions,
  TextInputProps as RNTextInputProps,
  TextInputSubmitEditingEventData,
} from 'react-native';
import {TextInputStyle} from './TextInputStyle';

export type VisualStyle = 'default' | 'simple' | 'material';

export interface TextInputProps {
  style?: TextInputStyle;
  placeholder?: string;
  value?: string;
  onChangeText?: (value: string) => void;
  inputType?: InputType;
  error?: string;
  editable?: boolean;
  leftImage?: ImageSourcePropType;
  rightImage?: ImageSourcePropType;
  visualStyle?: VisualStyle;
  multiline?: boolean;
  returnKeyType?: ReturnKeyTypeOptions;
  blurOnSubmit?: boolean;
  onSubmitEditing?: (e: NativeSyntheticEvent<TextInputSubmitEditingEventData>) => void;
  hint?: string;
  staticPlaceholder?: boolean;
  mask?: string;
  unmask?: boolean | 'typed';
  testID?: string;
}

export interface TextInputInternalPropsBase extends Omit<RNTextInputProps, 'style'> {
  placeholder: string | undefined;
  value: string | undefined;
  onChangeText: ((value: string) => void) | undefined;
  error: string | undefined;
  editable: boolean | undefined;
  autoCompleteType: AutoCompleteType | undefined;
  textContentType: TextContentType | undefined;
  keyboardType: KeyboardTypeOptions | undefined;
  autoCapitalize: AutoCapitalize | undefined;
  secureTextEntry: boolean | undefined;
  leftImage: ImageSourcePropType | undefined;
  rightImage: ImageSourcePropType | undefined;
  multiline: boolean | undefined;
  returnKeyType: ReturnKeyTypeOptions | undefined;
  blurOnSubmit: boolean | undefined;
  onSubmitEditing:
    | ((e: NativeSyntheticEvent<TextInputSubmitEditingEventData>) => void)
    | undefined;
  hint: string | undefined;
  staticPlaceholder: boolean | undefined;
  mask: string | undefined;
  unmask: boolean | 'typed' | undefined;
  testID: string | undefined;
}

export interface TextInputInternalProps extends TextInputInternalPropsBase {
  style: TextInputStyle | undefined;
}

export interface TextInputHandle {
  focus: () => void;
  isFocused: () => boolean;
}
