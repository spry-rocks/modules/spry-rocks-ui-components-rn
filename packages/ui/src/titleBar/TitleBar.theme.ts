import {TitleBarItemsStyle, TitleBarStyle} from './TitleBarStyle';

export interface TitleBarTheme {
  default?: TitleBarStyle;
}

export interface TitleBarItemsTheme {
  default?: TitleBarItemsStyle;
}
