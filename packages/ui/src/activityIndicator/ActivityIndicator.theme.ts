import {ActivityIndicatorStyle} from './ActivityIndicatorStyle';

export interface ActivityIndicatorTheme {
  default?: ActivityIndicatorStyle;
}
