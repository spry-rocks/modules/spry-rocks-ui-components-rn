/* eslint-disable @typescript-eslint/no-shadow,@typescript-eslint/no-explicit-any */
import {AutoCapitalize, AutoCompleteType, TextContentType} from './Model';
import React, {forwardRef, useImperativeHandle, useRef} from 'react';
import {TextInputHandle, TextInputInternalProps, TextInputProps} from './TextInput.props';
import {KeyboardTypeOptions} from 'react-native';

import TextInputDefault_ from './variants/Default';
import TextInputMaterial_ from './variants/Material';
import TextInputSimple_ from './variants/Simple';
import {wrapWithIMask} from './impl/IMaskWrapper';

const TextInputDefault = wrapWithIMask(TextInputDefault_);
const TextInputMaterial = wrapWithIMask(TextInputMaterial_);
const TextInputSimple = wrapWithIMask(TextInputSimple_);

const TextInput: React.ForwardRefRenderFunction<TextInputHandle, TextInputProps> = (
  {
    style,
    placeholder,
    value,
    onChangeText,
    inputType,
    error,
    editable,
    leftImage,
    rightImage,
    visualStyle = 'default',
    multiline,
    returnKeyType,
    blurOnSubmit,
    onSubmitEditing,
    hint,
    staticPlaceholder,
    mask,
    unmask,
    testID,
    ...restProps
  },
  ref,
) => {
  let autoCompleteType: AutoCompleteType | undefined;
  let textContentType: TextContentType | undefined;
  let keyboardType: KeyboardTypeOptions | undefined;
  let autoCapitalize: AutoCapitalize | undefined;
  let secureTextEntry: boolean | undefined;

  switch (inputType) {
    case 'email': {
      autoCompleteType = 'email';
      textContentType = 'emailAddress';
      keyboardType = 'email-address';
      autoCapitalize = 'none';
      break;
    }
    case 'password':
    case 'newPassword': {
      autoCompleteType = 'off';
      textContentType = 'none';
      autoCapitalize = 'none';
      secureTextEntry = true;
      break;
    }
    case 'username': {
      autoCompleteType = 'username';
      textContentType = 'username';
      autoCapitalize = 'words';
      break;
    }
    case 'phoneNumber': {
      keyboardType = 'phone-pad';
      textContentType = 'telephoneNumber';
    }
  }

  const textInputRef = useRef<TextInputHandle>();

  const internalProps: TextInputInternalProps = {
    style,
    value,
    leftImage,
    rightImage,
    editable,
    error,
    onChangeText,
    secureTextEntry,
    textContentType,
    autoCompleteType,
    autoCapitalize,
    keyboardType,
    placeholder,
    multiline,
    returnKeyType,
    blurOnSubmit,
    onSubmitEditing,
    hint,
    staticPlaceholder,
    mask,
    unmask,
    testID,
    ...restProps,
  };

  useImperativeHandle(ref, () => ({
    focus: () => {
      if (textInputRef.current) textInputRef.current?.focus();
    },
    isFocused: () => {
      if (textInputRef.current) return textInputRef?.current?.isFocused();
      return false;
    },
  }));

  switch (visualStyle) {
    case 'default':
      return (
        <TextInputDefault
          ref={(ref: any) => {
            textInputRef.current = ref ?? undefined;
          }}
          {...internalProps}
        />
      );
    case 'simple':
      return (
        <TextInputSimple
          ref={(ref: any) => {
            textInputRef.current = ref ?? undefined;
          }}
          {...internalProps}
        />
      );
    case 'material':
      return (
        <TextInputMaterial
          ref={(ref: any) => {
            textInputRef.current = ref ?? undefined;
          }}
          {...internalProps}
        />
      );
  }
};

export default forwardRef(TextInput);
