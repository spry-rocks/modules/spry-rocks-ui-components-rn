import Theme, {WithVariants} from './Theme';

export function mergeInternal(base: Theme, other: Theme) {
  const result = {...base};
  // eslint-disable-next-line guard-for-in
  for (const k in other) {
    const baseItem = base[k];
    const otherItem = other[k];
    if (
      baseItem !== null &&
      typeof baseItem === 'object' &&
      otherItem &&
      typeof otherItem === 'object'
    ) {
      result[k] = mergeInternal(baseItem as Theme, otherItem as Theme);
    } else {
      result[k] = otherItem;
    }
  }
  return result;
}

export function mergeThemes<T extends Theme>(base: T, other: T): Theme {
  return mergeInternal(base as T, other as T) as T;
}

export const mergeWithVariants = <TBaseTheme, TVariants extends string>(
  theme: WithVariants<TBaseTheme, TVariants> | undefined,
  ...includeVariants: TVariants[]
) => {
  if (!theme) return theme;
  let result: TBaseTheme = {
    ...theme,
  };
  includeVariants.forEach((variant) => {
    const variantTheme = theme.variant?.[variant];
    if (!variantTheme) return;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    Object.keys(variantTheme).forEach((key) => {
      result = {
        ...result,
        [key]: {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          ...result[key],
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          ...variantTheme[key],
        },
      };
    });
  });
  return result;
};
