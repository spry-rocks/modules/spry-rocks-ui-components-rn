import {ButtonStyle} from './ButtonStyle';
import {Theme} from '@spryrocks/rn-components-theme';

export interface ButtonDefaultTheme extends ButtonStyle {}

export interface ButtonRectTheme extends ButtonStyle {}

export interface ButtonSimpleTheme extends ButtonStyle {}

export interface ButtonTheme extends Theme {
  button?: {
    default?: ButtonDefaultTheme;
    rect?: ButtonRectTheme;
    simple?: ButtonSimpleTheme;
  };
}
