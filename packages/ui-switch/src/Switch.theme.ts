import {SwitchStyle} from './SwitchStyle';
import {Theme} from '@spryrocks/rn-components-theme';

export interface SwitchDefaultTheme {
  switch?: SwitchStyle;
  switchActive?: SwitchStyle;
}

export interface SwitchTheme extends Theme {
  switch?: {
    default?: SwitchDefaultTheme;
  };
}
