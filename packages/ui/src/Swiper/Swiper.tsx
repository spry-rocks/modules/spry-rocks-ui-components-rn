import React, {ForwardedRef, forwardRef, useImperativeHandle, useRef} from 'react';
import {ListRenderItem} from 'react-native';
import {SwiperFlatList} from 'react-native-swiper-flatlist';
import {SwiperStyle} from './SwiperStyle';
import {useStyles} from './Swiper.styles';

export interface SwiperHandle {
  scrollToIndex: (index: number, animated?: boolean) => void;
}

interface SwiperProps<T> {
  style?: SwiperStyle;
  index?: number;
  onChangeIndex?: (index: number) => void;
  data?: T[];
  renderItem?: ListRenderItem<T>;
}

function Swiper<T>(
  {style, index, onChangeIndex, data, renderItem}: SwiperProps<T>,
  ref: ForwardedRef<SwiperHandle>,
) {
  const swiperRef = useRef<SwiperFlatList>();
  const styles = useStyles({style});

  useImperativeHandle(ref, () => ({
    scrollToIndex: (index, animated = true) => {
      if (swiperRef.current) swiperRef.current?.scrollToIndex({index, animated});
    },
  }));

  return (
    <SwiperFlatList
      ref={(ref) => {
        swiperRef.current = ref ?? undefined;
      }}
      style={styles.swiper}
      index={index}
      onChangeIndex={({index}) => onChangeIndex && onChangeIndex(index)}
      data={data}
      renderItem={renderItem}
    />
  );
}

type ForwardRef = <T>(
  p: SwiperProps<T> & React.RefAttributes<SwiperHandle>,
) => React.ReactElement | null;
export default forwardRef(Swiper) as ForwardRef;
