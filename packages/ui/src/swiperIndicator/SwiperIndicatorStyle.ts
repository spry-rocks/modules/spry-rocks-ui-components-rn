import {ColorValue, ViewStyle} from 'react-native';

export default interface SwiperIndicatorStyle extends ViewStyle {
  color?: ColorValue;
  selectedColor?: ColorValue;
  selectedDotWidth?: number;
}
