export {default as StatusBar} from './StatusBar';
export type {StatusBarTheme} from './StatusBar.theme';
