import React, {useEffect, useRef, useState} from 'react';
import {ScrollView, View} from 'react-native';
import {useStyles} from './HorizontalPicker.styles';

interface HorizontalPickerProps<TItem> {
  itemSize: number;
  items: TItem[];
  renderItem: (props: {item: TItem; itemSize: number}) => React.ReactElement;
  selectedIndex: number;
  onSelectedIndexChanged: (index: number) => void;
}

function HorizontalPicker<TItem>({
  itemSize,
  items,
  renderItem,
  selectedIndex,
  onSelectedIndexChanged,
}: HorizontalPickerProps<TItem>) {
  const styles = useStyles(itemSize);
  const scrollViewRef = useRef<ScrollView>();
  const [contentWidth, setContentWidth] = useState<number>(0);
  const [scrollWidth, setScrollWidth] = useState<number>(0);

  const visibleItemsCountRaw = scrollWidth / itemSize;
  const visibleItemsCount = Math.floor(visibleItemsCountRaw);
  const extraItemsCount = Math.floor(Math.ceil(visibleItemsCountRaw) / 2);
  const itemOffset = (scrollWidth - visibleItemsCount * itemSize) / 2;

  const refreshScroll = () => {
    if (contentWidth > 0) {
      const offset =
        extraItemsCount * itemSize +
        selectedIndex * itemSize +
        itemSize / 2 -
        scrollWidth / 2;
      scrollViewRef.current?.scrollTo({x: offset});
    }
  };

  useEffect(() => {
    refreshScroll();
  }, [contentWidth, selectedIndex]);

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const renderCenterIndicator = () => (
    <View style={styles.centerItemIndicatorContainer}>
      <View style={styles.centerItemIndicatorBlock} />
    </View>
  );

  const renderExtraItem = () => <View style={styles.extraItem(extraItemsCount)} />;

  const onScrollEndDrag = ({x}: {x: number}) => {
    const itemX =
      x + itemSize * extraItemsCount - scrollWidth / 2 - itemSize / 2 + itemOffset * 2;
    let item = Math.round(itemX / itemSize);
    if (item < 0) item = 0;
    if (onSelectedIndexChanged) onSelectedIndexChanged(item);
  };

  return (
    <View>
      {/* {renderCenterIndicator()} */}
      <ScrollView
        ref={(r) => {
          scrollViewRef.current = r || undefined;
        }}
        style={styles.scrollView}
        horizontal
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        decelerationRate={0}
        snapToOffsets={items.map((v, i) => itemSize * i - itemOffset + itemSize / 2)}
        onLayout={(event) => {
          setScrollWidth(event.nativeEvent.layout.width);
        }}
        onScrollEndDrag={(event) => {
          onScrollEndDrag({x: event.nativeEvent.contentOffset.x});
        }}
      >
        <View
          style={styles.scrollViewContentContainer}
          onLayout={(event) => {
            setContentWidth(event.nativeEvent.layout.width);
          }}
        >
          {renderExtraItem()}
          {items.map((item) => renderItem({item, itemSize}))}
          {renderExtraItem()}
        </View>
      </ScrollView>
    </View>
  );
}

export default HorizontalPicker;
