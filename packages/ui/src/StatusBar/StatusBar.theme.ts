import {StatusBarStyle} from './StatusBarStyle';

export interface StatusBarTheme extends StatusBarStyle {}
