import {ViewStyle} from 'react-native';

export interface SwiperStyle {
  swiper?: ViewStyle;
}
