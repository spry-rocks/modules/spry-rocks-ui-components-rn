import {ImageStyle, TextStyle, ViewStyle} from 'react-native';
import {
  TouchableHighlightStyle,
  TouchableOpacityStyle,
} from '@spryrocks/rn-components-ui-touchable';

export interface ButtonStyle {
  container?: ViewStyle;
  containerDisabled?: ViewStyle;
  text?: TextStyle;
  textDisabled?: TextStyle;
  touchableHighlight?: TouchableHighlightStyle;
  touchableOpacity?: TouchableOpacityStyle;
  leftImage?: ImageStyle;
}
