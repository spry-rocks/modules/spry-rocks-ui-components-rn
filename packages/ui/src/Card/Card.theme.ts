import {CardStyle} from './CardStyle';

export interface CardTheme {
  default?: CardStyle;
}
