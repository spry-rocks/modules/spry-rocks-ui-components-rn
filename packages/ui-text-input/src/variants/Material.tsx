/* eslint-disable @typescript-eslint/no-shadow */
import {Image, View} from 'react-native';
import {MaterialInputRef, renderMaterialTextInput} from '../impl/MaterialTextInput';
import React, {forwardRef, useImperativeHandle, useRef, useState} from 'react';
import {
  TextInputHandle,
  TextInputInternalProps,
  TextInputInternalPropsBase,
} from '../TextInput.props';
import {Text} from '@spryrocks/rn-components-ui-text';
import {useOutlinedStyles} from '../TextInput.styles';

const TextInputMaterialRF: React.ForwardRefRenderFunction<
  TextInputHandle,
  TextInputInternalProps
> = ({error, style, rightImage, secureTextEntry, ...props}, ref) => {
  const [focused, setFocused] = useState<boolean>(false);
  const styles = useOutlinedStyles({error: !!error, style, focused});
  const textInputRef = useRef<MaterialInputRef>();
  useImperativeHandle(ref, () => ({
    focus: () => {
      if (textInputRef.current) textInputRef.current?.focus();
    },
    isFocused: () => true, // todo: refactoring
    setNativeProps: (_props: object) => {
      // if (textInputRef.current) textInputRef.current.setNativeProps(props); // todo: restore
    },
  }));
  return (
    <View style={styles.container}>
      <View style={styles.verticalContainer}>
        {renderMaterialTextInput({
          ...(props as TextInputInternalPropsBase),
          style: {
            container: styles.textInput.container,
            input: styles.textInput.input,
            inputFocused: styles.textInput.inputFocused,
            placeholder: styles.textInput.placeholder,
            placeholderFocused: styles.textInput.placeholderFocused,
          },
          focused,
          onFocused: (focused) => setFocused(focused),
          textInputRef: (ref) => {
            textInputRef.current = ref;
          },
          isPassword: secureTextEntry,
        })}
        {error && <Text style={styles.error.text}>{error}</Text>}
      </View>
      {rightImage && <Image source={rightImage} />}
    </View>
  );
};

export default forwardRef(TextInputMaterialRF);
