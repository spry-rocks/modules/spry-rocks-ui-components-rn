import {RatingStyle} from './RatingStyle';
import {ViewStyle} from 'react-native';

export interface RatingDefaultTheme {
  container?: ViewStyle;
  rating?: RatingStyle;
  ratingDisabled?: RatingStyle;
}

export interface RatingTheme {
  default?: RatingDefaultTheme;
}
