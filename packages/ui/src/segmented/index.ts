export {default as SegmentedControl} from './SegmentedControl';
export type {SegmentedControlTheme} from './SegmentedControl.theme';
export type {SegmentedControlStyle} from './SegmentedControlStyle';
