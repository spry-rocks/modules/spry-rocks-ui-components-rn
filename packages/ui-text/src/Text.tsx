import {Text as RNText, TextProps as RNTextProps, TextStyle} from 'react-native';
import React from 'react';
import {useStyles} from './Text.styles';

interface TextProps extends RNTextProps {
  style?: TextStyle;
  animated?: boolean;
}

const Text: React.FC<TextProps> = ({style, children, animated, ...props}) => {
  const styles = useStyles({style});

  if (animated) {
    const Animated = require('react-native-reanimated');
    return (
      <Animated.Text {...props} style={styles.text}>
        {children}
      </Animated.Text>
    );
  } else {
    return (
      <RNText {...props} style={styles.text}>
        {children}
      </RNText>
    );
  }
};

export default Text;
