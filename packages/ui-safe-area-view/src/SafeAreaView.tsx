import {ViewProps, ViewStyle} from 'react-native';
import React from 'react';
import {SafeAreaView as RNSACSafeAreaView} from 'react-native-safe-area-context';
import {useStyles} from './SafeAreaView.styles';

interface SafeAreaProps extends ViewProps {
  style?: ViewStyle;
}

const SafeAreaView: React.FC<SafeAreaProps> = ({style, ...otherProps}) => {
  const styles = useStyles();

  return (
    <RNSACSafeAreaView
      {...otherProps}
      style={{
        ...styles.safeArea,
        ...style,
      }}
    />
  );
};

export default SafeAreaView;
