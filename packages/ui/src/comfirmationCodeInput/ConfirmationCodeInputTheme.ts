import {ConfirmationCodeInputStyle} from './ConfirmationCodeInputStyle';

export interface ConfirmationCodeInputTheme {
  default?: ConfirmationCodeInputStyle;
}
